package io.ccs.service.modules;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import org.w3c.dom.Document;

import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;

public class ModuleEyeTrack implements Module {

	private ModuleAPI api = null;
	final int portNumber = 8080;
	private Socket socket;
	private BufferedReader br;


	private void ControlLogic(String cmd)
	{
		for (char c : cmd.toCharArray()) {
            		Set<Integer> commands = new HashSet<Integer>();
			switch (c) {
			case 's':
				commands.add(0);
				System.out.println("api call 0");
				break;
			case 'w':
				commands.add(1);
				System.out.println("api call 1");
				break;
			case 'a':
				commands.add(3);
				System.out.println("api call 3");
				break;
			case 'd':
				commands.add(4);
				System.out.println("api call 4");
				break;
			default:
				continue;
			}
			api.pushCommandOutput(commands);
		}
	}
		
	
	private void WaitForClient() throws IOException
	{
		ServerSocket serverSocket = null;
		serverSocket = new ServerSocket(portNumber);
		socket = serverSocket.accept();
		br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		
		System.out.println("Client is accepted");
		serverSocket.close();
	}

	private void MainLoop() throws IOException
	{
		while (true) {
			String command = br.readLine();
			System.out.println("Command: " + command);
			ControlLogic(command);
		}
	}

	private void Finalize() throws IOException
	{
		socket.close();
	}
	
	@Override
	public void run(ModuleAPI api) {
		this.api = api;
		System.out.println("Creating server socket on port " + portNumber);
		try {
			WaitForClient();
			MainLoop();
			Finalize();
		}
		catch (IOException e) { }

		System.out.println("EyeTrack interrupted");
	}

	@Override
	public Document query(Document request) {
		// TODO Auto-generated method stub
		return null;
	}

}
