package io.ccs.service.modules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import io.bci.Edk;
import io.bci.EdkErrorCode;
import io.bci.EmoState;
import io.bci.EmoState.IEE_MentalCommandAction_t;
import io.bci.PerformanceMetrics;
import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;
import io.ccs.service.core.Util;

public class ModuleBCI implements Module {
	IntByReference userID = new IntByReference(0);
	int PUSH = 0;
	IntByReference pXOut = new IntByReference();
	IntByReference pYOut = new IntByReference();
	int ROTATE = 0;
	int delta = 0;

	@Override
	public void run(ModuleAPI api) {
		String emoFileName = "";
		
		float powerCognitivPush = 0.0f;
		float powerCognitivPull = 0.0f;
		float powerCognitivRotateLeft = 0.0f;
		float powerCognitivRotateRight = 0.0f;
		float powerExpressivEyebrow = 0.0f;
		float powerExpressivSmile = 0.0f;
		float powerExpressivClench = 0.0f;
		float powerExpressivFurrow = 0.0f;
		float powerExpressivLaugh = 0.0f;
		float powerExpressivSmirkLeft = 0.0f;
		float powerExpressivSmirkRight = 0.0f;
		
		Map<String, String> conf = Util.documentToMap(api.getConfig());
		emoFileName = conf.get("emoFileName");
		powerCognitivPush = Float.parseFloat(conf.get("powerCognitivPush"));
		powerCognitivPull = Float.parseFloat(conf.get("powerCognitivPull"));
		powerCognitivRotateLeft = Float.parseFloat(conf.get("powerCognitivRotateLeft"));
		powerCognitivRotateRight = Float.parseFloat(conf.get("powerCognitivRotateRight"));
		powerExpressivEyebrow = Float.parseFloat(conf.get("powerExpressivEyebrow"));
		powerExpressivSmile = Float.parseFloat(conf.get("powerExpressivSmile"));
		powerExpressivClench = Float.parseFloat(conf.get("powerExpressivClench"));
		powerExpressivFurrow = Float.parseFloat(conf.get("powerExpressivFurrow"));
		powerExpressivLaugh = Float.parseFloat(conf.get("powerExpressivLaugh"));
		powerExpressivSmirkLeft = Float.parseFloat(conf.get("powerExpressivSmirkLeft"));
		powerExpressivSmirkRight = Float.parseFloat(conf.get("powerExpressivSmirkRight"));
		
		System.out.println("Emotiv starting...");
		if (Edk.INSTANCE.IEE_EngineConnect("Emotiv Systems-5") != EdkErrorCode.EDK_OK.ToInt()) {
			System.out.println("Emotiv Engine start up failed.");
			return;
		}
		System.out.println("Emotiv started.");
		Pointer eEvent = Edk.INSTANCE.IEE_EmoEngineEventCreate();
		while (!Thread.interrupted()) {
			int state = Edk.INSTANCE.IEE_EngineGetNextEvent(eEvent);
			if (state == EdkErrorCode.EDK_OK.ToInt()) {
				int eventType = Edk.INSTANCE.IEE_EmoEngineEventGetType(eEvent);
				IntByReference userID = new IntByReference(0);
				Edk.INSTANCE.IEE_EmoEngineEventGetUserId(eEvent, userID);
				if(eventType == Edk.IEE_Event_t.IEE_UserAdded.ToInt()) {
					Edk.INSTANCE.IEE_LoadUserProfile(userID.getValue(), emoFileName);
					/*byte[] data = null;
					try {
						data = Files.readAllBytes(Paths.get(emoFileName));
					} catch (IOException e) {
						System.out.println("Write right path pls ;)");
						e.printStackTrace();
					}
					Edk.INSTANCE.IEE_SetUserProfile(userID.getValue(), data, data.length);*/
					System.out.println("IEE_UserAdded."); 
				} else if(eventType == Edk.IEE_Event_t.IEE_EmoStateUpdated.ToInt()) {
					Pointer eState = Edk.INSTANCE.IEE_EmoStateCreate();
					Edk.INSTANCE.IEE_EmoEngineEventGetEmoState(eEvent, eState);
					
					Edk.INSTANCE.IEE_HeadsetGetGyroDelta(userID.getValue(), pXOut, pYOut);
					
					float excitementLong = PerformanceMetrics.INSTANCE.IS_PerformanceMetricGetExcitementLongTermScore(eState);
					float instantaneousExcitement = PerformanceMetrics.INSTANCE.IS_PerformanceMetricGetInstantaneousExcitementScore(eState);
					float relaxation = PerformanceMetrics.INSTANCE.IS_PerformanceMetricGetRelaxationScore(eState);
					//float stress = PerformanceMetrics.INSTANCE.IS_PerformanceMetricGetStressScore(eState);
					float engagementBoredom = PerformanceMetrics.INSTANCE.IS_PerformanceMetricGetEngagementBoredomScore(eState);											
					//float interest = PerformanceMetrics.INSTANCE.IS_PerformanceMetricGetInterestScore(eState);					
					float focus = PerformanceMetrics.INSTANCE.IS_PerformanceMetricGetFocusScore(eState);						
					
					Map<String, String> emotions = new HashMap<String, String>();
					emotions.put("engagementBoredom", Float.toString(engagementBoredom));
					emotions.put("excitementLongTerm", Float.toString(excitementLong));
					emotions.put("excitementShortTerm", Float.toString(instantaneousExcitement));
					emotions.put("frustration", Float.toString(focus));
					emotions.put("meditation", Float.toString(relaxation));
					List<Document> docList = new ArrayList<Document>();
					docList.add(Util.mapToDocument(emotions));
					Map<Integer, List<Document>> dataMap = new HashMap<Integer, List<Document>>();
					dataMap.put(0, docList);
					api.pushDataOutput(dataMap);
					
					int cognitivAction = EmoState.INSTANCE.IS_MentalCommandGetCurrentAction(eState);
					float cognitivPower = EmoState.INSTANCE.IS_MentalCommandGetCurrentActionPower(eState);
					
					Set<Integer> commandSet = new HashSet<Integer>();
					
					if ((cognitivAction & IEE_MentalCommandAction_t.MC_PUSH.ToInt()) > 0 && cognitivPower >= powerCognitivPush) {
						//commandSet.add(0);
						//System.out.println("COG_PUSH");
					}
					if ((cognitivAction & IEE_MentalCommandAction_t.MC_PULL.ToInt()) > 0 && cognitivPower >= powerCognitivPull) {
						commandSet.add(1);
						System.out.println("COG_PULL");
					}
					if ((cognitivAction & IEE_MentalCommandAction_t.MC_ROTATE_LEFT.ToInt()) > 0 && cognitivPower >= powerCognitivRotateLeft) {
						commandSet.add(2);
						System.out.println("COG_ROTATE_LEFT");
					}
					if ((cognitivAction & IEE_MentalCommandAction_t.MC_ROTATE_RIGHT.ToInt()) > 0 && cognitivPower >= powerCognitivRotateRight) {
						commandSet.add(3);
						System.out.println("COG_ROTATE_RIGHT");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionIsBlink(eState) == 1) {	
						commandSet.add(4);
						System.out.println("Blink");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionIsLeftWink(eState) == 1) {
						commandSet.add(5);
						System.out.println("LeftWink");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionIsRightWink(eState) == 1) {
						commandSet.add(6);
						System.out.println("RightWink");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionIsLookingLeft(eState) == 1) {
						commandSet.add(7);
						System.out.println("LookingLeft");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionIsLookingRight(eState) == 1) {
						commandSet.add(8);
						System.out.println("LookingRight");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionIsLookingRight(eState) >= powerExpressivEyebrow) { //IS_FacialExpressionGetSurpriseExtent
						commandSet.add(9);
						System.out.println("Eyebrow");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionGetSmileExtent(eState) >= powerExpressivSmile) {
						commandSet.add(10);
						System.out.println("Smile");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionGetClenchExtent(eState) >= powerExpressivClench) {
						commandSet.add(11);
						System.out.println("Clench");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionGetUpperFaceAction(eState) == EmoState.IEE_FacialExpressionAlgo_t.FE_FROWN.ToInt()
							&& EmoState.INSTANCE.IS_FacialExpressionGetUpperFaceActionPower(eState) >= powerExpressivFurrow) {
						commandSet.add(12);
						System.out.println("Furrow");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionGetLowerFaceAction(eState) == EmoState.IEE_FacialExpressionAlgo_t.FE_LAUGH.ToInt()
							&& EmoState.INSTANCE.IS_FacialExpressionGetLowerFaceActionPower(eState) >= powerExpressivLaugh) {
						commandSet.add(13);
						System.out.println("Laugh");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionGetLowerFaceAction(eState) == EmoState.IEE_FacialExpressionAlgo_t.FE_SMIRK_LEFT.ToInt()
							&& EmoState.INSTANCE.IS_FacialExpressionGetLowerFaceActionPower(eState) >= powerExpressivSmirkLeft) {
						commandSet.add(14);
						System.out.println("SmirkLeft");
					}
					if (EmoState.INSTANCE.IS_FacialExpressionGetLowerFaceAction(eState) == EmoState.IEE_FacialExpressionAlgo_t.FE_SMIRK_RIGHT.ToInt()
							&& EmoState.INSTANCE.IS_FacialExpressionGetLowerFaceActionPower(eState) >= powerExpressivSmirkRight) {
						commandSet.add(15);
						System.out.println("SmirkRight");
					}
					
					//	-1	0	1 ROTATE
					
					//delta += (int)pXOut.getValue();
		/*			if((int)pXOut.getValue() < -100) {
						System.out.println((int)pXOut.getValue());
						if(ROTATE == 1) {
							ROTATE = 0;
							commandSet.add(1);
							System.out.println("Stop1");
						}else if(ROTATE == 0) {
							ROTATE = -1;
							commandSet.add(3);
							System.out.println("Left");
						}
					}else if((int)pXOut.getValue() > 100) {
						System.out.println((int)pXOut.getValue());
						if(ROTATE == 0) {
							ROTATE = 1;
							commandSet.add(4);
							System.out.println("Right");
						}else if(ROTATE == -1) {
							ROTATE = 0;
							commandSet.add(1);
							System.out.println("Stop1");
						}
					}
			*/		

					if((int)pXOut.getValue() < -100) {
						System.out.println((int)pXOut.getValue());

					}else if((int)pXOut.getValue() > 100) {
						System.out.println((int)pXOut.getValue());
						if(ROTATE == 0) {
							ROTATE = 1;
							commandSet.add(4);
							System.out.println("Right");
						}else if(ROTATE == -1) {
							ROTATE = 0;
							commandSet.add(1);
							System.out.println("Stop1");
						}
					}
					
					if((int)pYOut.getValue() > 40) {
						if(PUSH == 1) {
							commandSet.add(1);
							System.out.println("Stop");
							PUSH = 0;
						}
					}else if((int)pYOut.getValue() < -40) { 
						commandSet.add(0);
						PUSH = 1;
						System.out.println("Push");
					}
					
					api.pushCommandOutput(commandSet);
				}
			} else if (state != EdkErrorCode.EDK_NO_EVENT.ToInt()) {
				System.out.println("Internal error in Emotiv Engine!");
				break;
			} /*else {
				System.out.println("wtf????");
				break;
			}*/
		}
	}

	@Override
	public Document query(Document request) {
		// TODO Auto-generated method stub
		return null;
	}
}