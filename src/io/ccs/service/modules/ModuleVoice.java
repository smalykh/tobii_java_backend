package io.ccs.service.modules;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.w3c.dom.Document;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;

public class ModuleVoice implements Module {

	private class SphinxWrapper implements Runnable {

		private LiveSpeechRecognizer jsgfRecognizer = null;
		private ModuleAPI api = null;
		
		public SphinxWrapper(LiveSpeechRecognizer jsgfRecognizer, ModuleAPI api) {
			this.jsgfRecognizer = jsgfRecognizer;
			this.api = api;
		}
		
		@Override
		public void run() {
            while (!Thread.interrupted()) {
            	String utterance = jsgfRecognizer.getResult().getHypothesis();
            	Set<Integer> commands = new HashSet<Integer>();
            	if (utterance.equals("kreslo stoi")) {
            		commands.add(0);
	            	System.out.println("kreslo stoi"); // TODO: to log
	            } else if (utterance.equals("kreslo vpered")) {
	            	commands.add(1);
	            	System.out.println("kreslo vpered"); // TODO: to log   	
	            } else if (utterance.equals("kreslo nazad")) {
	            	commands.add(2);
	            	System.out.println("kreslo nazad"); // TODO: to log
	            } else if (utterance.equals("kreslo nalevo")) {
	            	commands.add(3);
	            	System.out.println("kreslo nalevo"); // TODO: to log
	            } else if (utterance.equals("kreslo napravo")) {
	            	commands.add(4);
	            	System.out.println("kreslo napravo"); // TODO: to log
	            }
            	api.pushCommandOutput(commands);
            	System.out.println("utterance "  + utterance); // TODO: to log
            }
		}
		
	}
	
	private static final String DIALOG = "dialog";
	private static final String ACOUSTIC_MODEL = "sphinx_files/en-us";
	private static final String DICTIONARY_PATH = "sphinx_files/cmudict-en-us-2.dict";
	private static final String GRAMMAR_PATH = "sphinx_files/";
	
	@SuppressWarnings("deprecation")
	@Override
	public void run(ModuleAPI api) {
		Configuration configuration = new Configuration();
        configuration.setAcousticModelPath(ACOUSTIC_MODEL);
        configuration.setDictionaryPath(DICTIONARY_PATH);
        configuration.setGrammarPath(GRAMMAR_PATH);
        configuration.setUseGrammar(true);
        configuration.setGrammarName(DIALOG);
        LiveSpeechRecognizer jsgfRecognizer = null;
		try {
			jsgfRecognizer = new LiveSpeechRecognizer(configuration);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        SphinxWrapper sphinxWrapper = new SphinxWrapper(jsgfRecognizer, api);
        Thread thread = new Thread(sphinxWrapper);
        jsgfRecognizer.startRecognition(true);
        thread.start();
        try {
			thread.join();
		} catch (InterruptedException e) {
		}
        try {
        	jsgfRecognizer.stopRecognition();
        } catch(IllegalStateException e) {
        }
        thread.stop();
        System.out.println("Voice interrupted"); // TODO: to log
	}

	@Override
	public Document query(Document request) {
		// TODO Auto-generated method stub
		return null;
	}

}
