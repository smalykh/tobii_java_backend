package io.ccs.service.modules;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;
import io.ccs.service.core.Util;

public class ModuleJoystick implements Module {
	
	private String port = "COM1";
	private int timeout = 1000;
	private int rate = 9600;

	@Override
	public void run(ModuleAPI api) {
		Map<String, String> config = Util.documentToMap(api.getConfig());
		if (config.containsKey("port")) {
			port = config.get("port");
		}
		if (config.containsKey("timeout")) {
			timeout = Integer.parseInt(config.get("timeout"));
		}
		if (config.containsKey("rate")) {
			rate = Integer.parseInt(config.get("rate"));
		}
		SerialPort serialPort = null;
		try {
			while (serialPort == null) {
				@SuppressWarnings("rawtypes")
				Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
				while (portEnum.hasMoreElements()) {
					CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
					if (currPortId.getName().equals(port) || currPortId.getName().startsWith(port)) { // startsWith ???
						try {
							serialPort = (SerialPort) currPortId.open(getClass().getName(), timeout);
							serialPort.setSerialPortParams(rate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
						} catch (PortInUseException | UnsupportedCommOperationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("Connected on port" + currPortId.getName()); // TODO: to log
						break;
					}
				}
				if (serialPort == null) {
					Thread.sleep(timeout);
				} else {
					break;
				}
			}
			int ARDUINO_SLEEP_TIME = 2000;
			Thread.sleep(ARDUINO_SLEEP_TIME);
			byte[] b = new byte[1];
			int x = 7;
			int y = 7;
			while (true) {
				Set<Integer> commands = api.popCommandInputPoll();
				for (int command : commands) {
					switch (command) {
					case 0:
						x = 7;
						y = 7;
						break;
					case 1:
						x = 9;	//14 is max
						break;
					case 2:
						x = 3;	//0 is min
						break;
					case 3:
						y = 11;	//14 is max
						break;
					case 4:
						y = 3;	//0 is min
						break;
					}
				}
				b[0] = (byte) (y*16 + x);
				try {
					serialPort.getOutputStream().write(b);
				} catch (IOException e) {
					e.printStackTrace();
				}
				Thread.sleep(1);
			}
		} catch (InterruptedException e) {
			System.out.println("Joystick interrupted");
			return;
		}
	}

	@Override
	public Document query(Document request) {
		// TODO Auto-generated method stub
		return null;
	}

}
