package io.ccs.service.modules;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.mephi.decisionmakingsystem.DecisionMakingSystem;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;
import io.ccs.service.core.Util;

public class ModuleDecisionMakingSystem implements Module {
	
	private int channels = 0;
	private int commands = 0;
	private long[][] executionTime = null;
	private double[][] minEmotion = null;
	private double[][] priority = null;
	private DecisionMakingSystem dms = null;
	
	@Override
	public void run(ModuleAPI api) {
		channels = 0;
		commands = 0;
		Document config = api.getConfig();
		NodeList nodeList = config.getDocumentElement().getElementsByTagName("value");
		for (int n = 0; n < nodeList.getLength(); ++n) {
			Element e = (Element) nodeList.item(n);
			String name = e.getAttribute("name");
			if (name.equals("channels")) {
				channels = Integer.parseInt(e.getTextContent());
			} else if (name.equals("commands")) {
				commands = Integer.parseInt(e.getTextContent());
			}
		}
		executionTime = new long[channels][commands];
		minEmotion = new double[channels][commands];
		priority = new double[channels][commands];
		for (int i = 0; i < channels; ++i) {
			for (int j = 0; j < commands; ++j) {
				executionTime[i][j] = 1000;
				minEmotion[i][j] = 0.5;
				priority[i][j] = 0.0;
			}
		}
		Element elementChannels = (Element) config.getDocumentElement().getElementsByTagName("channels").item(0);
		NodeList nodeListChannels = elementChannels.getElementsByTagName("channel");
		for (int n = 0; n < nodeListChannels.getLength(); ++n) {
			Element elementChannel = (Element) nodeListChannels.item(n);
			int i = Integer.parseInt(elementChannel.getAttribute("id"));
			NodeList nodeListChannelCommands = elementChannel.getElementsByTagName("command");
			for (int m = 0; m < nodeListChannelCommands.getLength(); ++m) {
				Element elementChannelCommand = (Element) nodeListChannelCommands.item(m);
				int j = Integer.parseInt(elementChannelCommand.getAttribute("id"));
				executionTime[i][j] = Long.parseLong(elementChannelCommand.getAttribute("executionTime"));
				minEmotion[i][j] = Double.parseDouble(elementChannelCommand.getAttribute("minEmotion"));
				priority[i][j] = Double.parseDouble(elementChannelCommand.getAttribute("priority"));
			}
		}
		List<Set<Integer>> groups = new ArrayList<Set<Integer>>();
		Element elementGroups = (Element) config.getDocumentElement().getElementsByTagName("groups").item(0);
		NodeList nodeListGroups = elementGroups.getElementsByTagName("group");
		for (int n = 0; n < nodeListGroups.getLength(); ++n) {
			Element elementGroup = (Element) nodeListGroups.item(n);
			Set<Integer> group = new HashSet<Integer>();
			NodeList nodeListGroupCommands = elementGroup.getElementsByTagName("command");
			for (int m = 0; m < nodeListGroupCommands.getLength(); ++m) {
				Element elementGroupCommand = (Element) nodeListGroupCommands.item(m);
				int j = Integer.parseInt(elementGroupCommand.getAttribute("id"));
				group.add(j);
			}
			groups.add(group);
		}
				
		Set<Integer> active = new HashSet<Integer>();
		double emotion = 1.0;
		
		dms = new DecisionMakingSystem(channels, commands, groups, executionTime, minEmotion, priority);
		while (true) {
			Set<Integer> chcmds = api.popCommandInputPoll();
			
			Map<Integer, List<Document>> data = api.popDataInputPoll();
			if (data.containsKey(0)) {
				List<Document> dList = data.get(0);
				if (dList.size() > 0) {
					Map<String, String> emotions = Util.documentToMap(dList.get(dList.size() - 1));
					double engagementBoredom = Double.parseDouble(emotions.get("engagementBoredom"));
					double excitementLongTerm = Double.parseDouble(emotions.get("excitementLongTerm"));
					double excitementShortTerm = Double.parseDouble(emotions.get("excitementShortTerm"));
					double frustration = Double.parseDouble(emotions.get("frustration"));
					double meditation = Double.parseDouble(emotions.get("meditation"));
					emotion = (engagementBoredom + meditation)/(engagementBoredom + excitementLongTerm + excitementShortTerm + frustration + meditation);
				}
			}
			
			for (int chcmd : chcmds) {
				int ch = chcmd / commands;
				int cmd = chcmd % commands;
				dms.activate(ch, cmd, emotion);
			}
			Set<Integer> currentAcive = dms.active();
			Set<Integer> toSend = new HashSet<Integer>();
			toSend.addAll(currentAcive);
			toSend.removeAll(active);
			active = currentAcive;
			api.pushCommandOutput(toSend);
			long dt = 10;
			dms.sleep(dt);
			try {
				Thread.sleep(dt);
			} catch (InterruptedException e) {
				break;
			}
		}
	}

	@Override
	public Document query(Document request) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			return null;
		}
		Document document = builder.newDocument();
		Element rootElement = document.createElement("response");
		document.appendChild(rootElement);
		
		String type = request.getDocumentElement().getAttribute("type");
		if (type == null || type.equals("all") || type.equals("config")) {
			Element elementConfig = document.createElement("config");
			rootElement.appendChild(elementConfig);

			Element elementValueChannels = document.createElement("value");
			elementConfig.appendChild(elementValueChannels);
			Attr attrValueChannels = document.createAttribute("name");
			attrValueChannels.setValue("channels");
			elementValueChannels.setAttributeNode(attrValueChannels);
			elementValueChannels.appendChild(document.createTextNode(Integer.toString(channels)));
			
			Element elementValueCommands = document.createElement("value");
			elementConfig.appendChild(elementValueCommands);
			Attr attrValueCommands = document.createAttribute("name");
			attrValueCommands.setValue("commands");
			elementValueCommands.setAttributeNode(attrValueCommands);
			elementValueCommands.appendChild(document.createTextNode(Integer.toString(commands)));
			
			Element elementChannels = document.createElement("channels");
			elementConfig.appendChild(elementChannels);
			for (int i = 0; i < channels; ++i) {
				Element elementChannel = document.createElement("channel");
				elementChannels.appendChild(elementChannel);
				
				Attr attrChannelId = document.createAttribute("id");
				attrChannelId.setValue(Integer.toString(i));
				elementChannel.setAttributeNode(attrChannelId);
				
				Element elementChannelCommands = document.createElement("commands");
				elementChannel.appendChild(elementChannelCommands);
				for (int j = 0; j < commands; ++j) {
					Element elementChannelCommand = document.createElement("command");
					elementChannelCommands.appendChild(elementChannelCommand);
					
					Attr attrCommandId = document.createAttribute("id");
					attrCommandId.setValue(Integer.toString(j));
					elementChannelCommand.setAttributeNode(attrCommandId);
					
					Element elementValueExecutionTime = document.createElement("value");
					elementChannelCommand.appendChild(elementValueExecutionTime);
					Attr attrValueExecutionTime = document.createAttribute("name");
					attrValueExecutionTime.setValue("executionTime");
					elementValueExecutionTime.setAttributeNode(attrValueExecutionTime);
					elementValueExecutionTime.appendChild(document.createTextNode(Long.toString(executionTime[i][j])));
					
					Element elementValueMinEmotion = document.createElement("value");
					elementChannelCommand.appendChild(elementValueMinEmotion);
					Attr attrValueMinEmotion = document.createAttribute("name");
					attrValueMinEmotion.setValue("minEmotion");
					elementValueMinEmotion.setAttributeNode(attrValueExecutionTime);
					elementValueMinEmotion.appendChild(document.createTextNode(Double.toString(minEmotion[i][j])));
					
					Element elementValuePriority = document.createElement("value");
					elementChannelCommand.appendChild(elementValuePriority);
					Attr attrValuePriority = document.createAttribute("name");
					attrValuePriority.setValue("priority");
					elementValuePriority.setAttributeNode(attrValuePriority);
					elementValuePriority.appendChild(document.createTextNode(Double.toString(priority[i][j])));
				}
			}
		}
		if (type == null || type.equals("all") || type.equals("state")) {
			long time = dms.getTime();
			long[][] endTimeBackground = dms.getEndTimeBackground();
			long[] endTimeForeground = dms.getEndTimeForeground();
			
			Element elementState = document.createElement("state");
			rootElement.appendChild(elementState);

			Element elementValueTime = document.createElement("value");
			elementState.appendChild(elementValueTime);
			Attr attrValueTime = document.createAttribute("name");
			attrValueTime.setValue("time");
			elementValueTime.setAttributeNode(attrValueTime);
			elementValueTime.appendChild(document.createTextNode(Long.toString(time)));
			
			Element elementCommands = document.createElement("commands");
			elementState.appendChild(elementCommands);
			for (int j = 0; j < commands; ++j) {
				Element elementCommand = document.createElement("command");
				elementCommands.appendChild(elementCommand);
				
				Attr attrCommandId = document.createAttribute("id");
				attrCommandId.setValue(Integer.toString(j));
				elementCommand.setAttributeNode(attrCommandId);
				
				Element elementValueEndTimeForeground = document.createElement("value");
				elementCommand.appendChild(elementValueEndTimeForeground);
				Attr attrValueEndTimeForeground = document.createAttribute("name");
				attrValueEndTimeForeground.setValue("endTimeForeground");
				elementValueEndTimeForeground.setAttributeNode(attrValueEndTimeForeground);
				elementValueEndTimeForeground.appendChild(document.createTextNode(Long.toString(endTimeForeground[j])));
			}
			
			Element elementChannels = document.createElement("channels");
			elementState.appendChild(elementChannels);
			for (int i = 0; i < channels; ++i) {
				Element elementChannel = document.createElement("channel");
				elementChannels.appendChild(elementChannel);
				
				Attr attrChannelId = document.createAttribute("id");
				attrChannelId.setValue(Integer.toString(i));
				elementChannel.setAttributeNode(attrChannelId);
				
				Element elementChannelCommands = document.createElement("commands");
				elementChannel.appendChild(elementChannelCommands);
				for (int j = 0; j < commands; ++j) {
					Element elementChannelCommand = document.createElement("command");
					elementChannelCommands.appendChild(elementChannelCommand);
					
					Attr attrCommandId = document.createAttribute("id");
					attrCommandId.setValue(Integer.toString(j));
					elementChannelCommand.setAttributeNode(attrCommandId);
					
					Element elementValueEndTimeBackground = document.createElement("value");
					elementChannelCommand.appendChild(elementValueEndTimeBackground);
					Attr attrValueEndTimeBackground = document.createAttribute("name");
					attrValueEndTimeBackground.setValue("endTimeBackground");
					elementValueEndTimeBackground.setAttributeNode(attrValueEndTimeBackground);
					elementValueEndTimeBackground.appendChild(document.createTextNode(Long.toString(endTimeBackground[i][j])));
				}
			}
		}
		return document;
	}

}
