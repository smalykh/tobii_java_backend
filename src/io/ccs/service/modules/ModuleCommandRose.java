package io.ccs.service.modules;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.mephi.commandrose.CommandRose;
import org.w3c.dom.Document;

import intel.rssdk.PXCMCaptureManager;
import intel.rssdk.PXCMHandConfiguration;
import intel.rssdk.PXCMHandData;
import intel.rssdk.PXCMHandModule;
import intel.rssdk.PXCMPoint3DF32;
import intel.rssdk.PXCMSenseManager;
import intel.rssdk.PXCMSession;
import intel.rssdk.pxcmStatus;
import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;
import io.ccs.service.core.Util;

public class ModuleCommandRose implements Module {
	
	private int nCommands = 0;
	private double centerX = 0.0;
	private double centerY = 0.0;
	private double radiusBegin = 0.0;
	private double radiusEndInner = 0.0;
	private double radiusEndOuter = 0.0;
	private double radiusMax = 0.0;
	private double sectorOffset = 0.0;
	private double sectorAngle = 0.0;
	
	private boolean pointerSet = false;
	private double pointerX = 0.0;
	private double pointerY = 0.0;
	
	@Override
	public void run(ModuleAPI api) {
		// TODO Auto-generated method stub
		
		// TODO: load this shit from config !!!!
		nCommands = 4;
		centerX = -0.04;
		centerY = 0.0;
		radiusBegin = 0.02;
		radiusEndInner = 0.03;
		radiusEndOuter = 0.06;
		radiusMax = 0.07;
		sectorOffset = 0.0;
		sectorAngle = 80.0 * Math.PI / 180.0;		
		
		CommandRose commandRose = new CommandRose(nCommands, centerX, centerY,
				radiusBegin, radiusEndInner, radiusEndOuter, radiusMax, sectorOffset, sectorAngle);
		
		// Create session
		PXCMSession session = PXCMSession.CreateInstance();
		if (session == null) {
			System.out.print("Failed to create a session instance\n");
			return; // TODO: try to restart the module
		}
		
		PXCMSenseManager senseManager = session.CreateSenseManager();
		if (senseManager == null) {
			System.out.print("Failed to create a SenseManager instance\n");
			return; // TODO: try to restart the module
		}
		
		PXCMCaptureManager captureMgr = senseManager.QueryCaptureManager();
		captureMgr.FilterByDeviceInfo("RealSense", null, 0);
		
		pxcmStatus status = senseManager.EnableHand(null);
		if (status.compareTo(pxcmStatus.PXCM_STATUS_NO_ERROR)<0) {
			System.out.print("Failed to enable HandAnalysis\n");
			return; // TODO: try to restart the module
		}
		
		status = senseManager.Init();
		if (status.compareTo(pxcmStatus.PXCM_STATUS_NO_ERROR)>=0) {
			PXCMHandModule handModule = senseManager.QueryHand(); 
			PXCMHandConfiguration handConfig = handModule.CreateActiveConfiguration();
			handConfig.Update();
			
			PXCMHandData handData = handModule.CreateOutput();
			
			while(!Thread.interrupted()) {         	
				status = senseManager.AcquireFrame(true);
				if (status.compareTo(pxcmStatus.PXCM_STATUS_NO_ERROR)<0) {
					break;
				}

				handData.Update(); 
				
				if (handData.QueryNumberOfHands() == 0) {
					
					pointerSet = false;
					
					Set<Integer> out = new HashSet<Integer>();
					out.add(commandRose.process());
					api.pushCommandOutput(out);
				}

				PXCMHandData.IHand hand = new PXCMHandData.IHand();
				status = handData.QueryHandData(PXCMHandData.AccessOrderType.ACCESS_ORDER_NEAR_TO_FAR, 0, hand);
				
				PXCMHandData.JointData data = new PXCMHandData.JointData();
				
				if (status.compareTo(pxcmStatus.PXCM_STATUS_NO_ERROR) >= 0) {
					hand.QueryTrackedJoint(PXCMHandData.JointType.JOINT_CENTER, data);
					PXCMPoint3DF32 world = hand.QueryMassCenterWorld();
					double worldX = world.x;
					double worldY = world.y;
					
					pointerSet = true;
					pointerX = worldX;
					pointerY = worldY;
					
					Set<Integer> out = new HashSet<Integer>();
					out.add(commandRose.process(worldX, worldY));
					api.pushCommandOutput(out);
				}
				senseManager.ReleaseFrame();
			}
			System.out.print("command rose succesfully stopped\n");
			return; // Module must stop when it's thread is interrupted!
		} else {
			System.out.print("senseManager.Init() failed\n");
			return; // TODO: try to restart the module
		}
	}

	@Override
	public Document query(Document request) {
		Map<String, String> dataMap = new HashMap<String, String>();
		dataMap.put("n", Integer.toString(nCommands));
		dataMap.put("centerX", Double.toString(centerX));
		dataMap.put("centerY", Double.toString(centerY));
		dataMap.put("radiusBegin", Double.toString(radiusBegin));
		dataMap.put("radiusEndInner", Double.toString(radiusEndInner));
		dataMap.put("radiusEndOuter", Double.toString(radiusEndOuter));
		dataMap.put("radiusMax", Double.toString(radiusMax));
		dataMap.put("sectorOffset", Double.toString(sectorOffset));
		dataMap.put("sectorAngle", Double.toString(sectorAngle));
		dataMap.put("pointerSet", Boolean.toString(pointerSet));
		dataMap.put("pointerX", Double.toString(pointerX));
		dataMap.put("pointerY", Double.toString(pointerY));
		return Util.mapToDocument(dataMap, "response", "value", "name");
	}

}
