package io.ccs.service.modules;

import java.util.Set;

import org.w3c.dom.Document;

import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;

public class ModuleDebugOutput implements Module  {

	@Override
	public void run(ModuleAPI api) {
		while(true) {
			Set<Integer> in;
			try {
				in = api.popCommandInputWait();
			} catch (InterruptedException e) {
				return;
			}
			for (int i : in) {
				System.out.println("DEBUG OUTPUT: " + i);
			}
		}
		
	}

	@Override
	public Document query(Document request) {
		// TODO Auto-generated method stub
		return null;
	}

}
