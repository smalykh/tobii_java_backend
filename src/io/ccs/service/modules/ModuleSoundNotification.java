package io.ccs.service.modules;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.mephi.soundnotification.SoundEvent;
import org.mephi.soundnotification.SoundNotification;
import org.w3c.dom.Document;

import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;

public class ModuleSoundNotification implements Module {

	@Override
	public void run(ModuleAPI api) {
		Map<Integer, SoundEvent> map = new HashMap<Integer, SoundEvent>();
		map.put(0, new SoundEvent("sounds/stop.wav", 0, false, false));
		map.put(1, new SoundEvent("sounds/forward.wav", 0, false, false));
		map.put(2, new SoundEvent("sounds/backward.wav", 0, false, false));
		map.put(3, new SoundEvent("sounds/left.wav", 0, false, false));
		map.put(4, new SoundEvent("sounds/right.wav", 0, false, false));
		map.put(5, new SoundEvent("sounds/hand_lost.wav", 10, false, false));
		map.put(6, new SoundEvent("sounds/hand_found.wav", 10, false, false));
		map.put(7, new SoundEvent("sounds/hand_start.wav", 10, false, false));
		map.put(100, new SoundEvent("sounds/john.wav", 100, false, false));
		SoundNotification soundNotification = new SoundNotification(map);
		soundNotification.activate(100);
		while (true) {
			try {
				Set<Integer> in = api.popCommandInputWait();
				for (int i : in) {
					soundNotification.activate(i);
				}
			} catch (InterruptedException e) {
				soundNotification.interrupt();
				return;
			}
		}
	}

	@Override
	public Document query(Document request) {
		// TODO Auto-generated method stub
		return null;
	}

}
