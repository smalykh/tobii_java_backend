package io.ccs.service.modules;

import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleFactory;

public class Factory implements ModuleFactory {

	public static final int MODULE_JOYSTICK = 0;
	public static final int MODULE_DMS = 1;
	public static final int MODULE_VOICE = 101;
	public static final int MODULE_COMMANDROSE = 102;
	public static final int MODULE_BCI = 103;
	public static final int MODULE_EYETRACK = 104;
	public static final int MODULE_SOUNDNOTIFICATION = 201;
	public static final int MODULE_PROXY = 301;
	public static final int MODULE_DEBUGOUTPUT = 1000;
	
	@Override
	public Module make(int type) {
		switch (type) {
		case MODULE_JOYSTICK:
			return new ModuleJoystick();
		case MODULE_DMS:
			return new ModuleDecisionMakingSystem();
		case MODULE_VOICE:
			return new ModuleVoice();
		case MODULE_COMMANDROSE:
			return new ModuleCommandRose();
		case MODULE_BCI:
			return new ModuleBCI();
		case MODULE_EYETRACK:
			return new ModuleEyeTrack();
		case MODULE_SOUNDNOTIFICATION:
			return new ModuleSoundNotification();
		case MODULE_PROXY:
			return new ModuleProxy();
		case MODULE_DEBUGOUTPUT:
			return new ModuleDebugOutput();
		}
		return null;
	}

}
