package io.ccs.service.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Router {
	
	private class ModuleSlot {
		public int module;
		public int slot;
		public ModuleSlot(int module, int slot) {
			this.module = module;
			this.slot = slot;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + module;
			result = prime * result + slot;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ModuleSlot other = (ModuleSlot) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (module != other.module)
				return false;
			if (slot != other.slot)
				return false;
			return true;
		}
		private Router getOuterType() {
			return Router.this;
		}
	}

	private final ModuleFactory moduleFactory;
	
	private final Map<Integer, ModuleThread> modules = new HashMap<Integer, ModuleThread>();
	private final Map<Integer, Map<Integer, Set<ModuleSlot>>> routeCommand = new HashMap<Integer, Map<Integer, Set<ModuleSlot>>>();
	private final Map<Integer, Map<Integer, Set<ModuleSlot>>> routeData = new HashMap<Integer, Map<Integer, Set<ModuleSlot>>>();
	private final Map<Integer, Set<Integer>> blockedCommandFrom = new HashMap<Integer, Set<Integer>>();
	private final Map<Integer, Set<Integer>> blockedCommandTo = new HashMap<Integer, Set<Integer>>();
	private final Map<Integer, Set<Integer>> blockedDataFrom = new HashMap<Integer, Set<Integer>>();
	private final Map<Integer, Set<Integer>> blockedDataTo = new HashMap<Integer, Set<Integer>>();

	public Router(Document config, ModuleFactory moduleFactory) {
		this.moduleFactory = moduleFactory;
		
		Element modulesElement = (Element) config.getDocumentElement().getElementsByTagName("modules").item(0);
		NodeList modulesNodeList = modulesElement.getElementsByTagName("module");
		for (int i = 0; i < modulesNodeList.getLength(); ++i) {
			Element element = (Element) modulesNodeList.item(i);
			int id = Integer.parseInt(element.getAttribute("id"));
			int type = Integer.parseInt(element.getAttribute("type"));
			Document document = Util.elementToDocument((Element) element.getElementsByTagName("config").item(0));
			addModule(id, type, document);
		}
		
		Element connectionsElement = (Element) config.getDocumentElement().getElementsByTagName("connections").item(0);
		
		Element connectionsCommandElement = (Element) connectionsElement.getElementsByTagName("command").item(0);
		NodeList connectionsCommandNodeList = connectionsCommandElement.getElementsByTagName("connection");
		for (int i = 0; i < connectionsCommandNodeList.getLength(); ++i) {
			Element element = (Element) connectionsCommandNodeList.item(i);
			int fromModule = Integer.parseInt(element.getAttribute("fromModule"));
			int fromSlot = Integer.parseInt(element.getAttribute("fromSlot"));
			int toModule = Integer.parseInt(element.getAttribute("toModule"));
			int toSlot = Integer.parseInt(element.getAttribute("toSlot"));
			addRouteCommand(fromModule, fromSlot, toModule, toSlot);
		}
		
		Element connectionsDataElements = (Element) connectionsElement.getElementsByTagName("data").item(0);
		NodeList connectionsDataNodeList = connectionsDataElements.getElementsByTagName("connection");
		for (int i = 0; i < connectionsDataNodeList.getLength(); ++i) {
			Element element = (Element) connectionsDataNodeList.item(i);
			int fromModule = Integer.parseInt(element.getAttribute("fromModule"));
			int fromSlot = Integer.parseInt(element.getAttribute("fromSlot"));
			int toModule = Integer.parseInt(element.getAttribute("toModule"));
			int toSlot = Integer.parseInt(element.getAttribute("toSlot"));
			addRouteData(fromModule, fromSlot, toModule, toSlot);
		}
	}

	public void start() {
		for (Entry<Integer, ModuleThread> entry : modules.entrySet()) {
			entry.getValue().start();
		}
	}

	public void interrupt() {
		for (Entry<Integer, ModuleThread> entry : modules.entrySet()) {
			entry.getValue().interrupt();
		}
	}

	public void join() throws InterruptedException {
		for (Entry<Integer, ModuleThread> entry : modules.entrySet()) {
			entry.getValue().join();
		}
	}

	public void addModule(int id, int type, Document config) {
		modules.put(id, new ModuleThread(this, id, moduleFactory.make(type), config));
	}
	
	public void addRouteCommand(int fromModule, int fromSlot, int toModule, int toSlot) {
		addRoute(routeCommand, fromModule, fromSlot, toModule, toSlot);
	}

	public void addRouteData(int fromModule, int fromSlot, int toModule, int toSlot) {
		addRoute(routeData, fromModule, fromSlot, toModule, toSlot);
	}
	
	private void addRoute(Map<Integer, Map<Integer, Set<ModuleSlot>>> route, int fromModule, int fromSlot, int toModule, int toSlot) {
		Map<Integer, Set<ModuleSlot>> map = route.get(fromModule);
		if (map == null) {
			map = new HashMap<Integer, Set<ModuleSlot>>();
			route.put(fromModule, map);
		}
		Set<ModuleSlot> set = map.get(fromSlot);
		if (set == null) {
			set = new HashSet<ModuleSlot>();
			map.put(fromSlot, set);
		}
		ModuleSlot moduleSlot = new ModuleSlot(toModule, toSlot);
		if (!set.contains(moduleSlot)) {
			set.add(moduleSlot);
		}
	}
	
	public void sendCommandFrom(int id, Set<Integer> commands) {
		for (int command : commands) {
			if (!blockedCommandFrom.containsKey(id) || !blockedCommandFrom.get(id).contains(command)) {
				Map<Integer, Set<ModuleSlot>> map = routeCommand.get(id);
				if (map != null && map.get(command) != null) { // TODO: here!!!
					for (ModuleSlot moduleSlot : routeCommand.get(id).get(command)) {
						Set<Integer> set = new HashSet<Integer>();
						set.add(moduleSlot.slot);
						sendCommandTo(moduleSlot.module, set);
					}
				}
			}
		}
	}
	
	public void sendCommandTo(int id, Set<Integer> commands) {
		Set<Integer> cmds;
		if (blockedCommandTo.containsKey(id)) {
			cmds = new HashSet<Integer>(commands);
			cmds.removeAll(blockedCommandTo.get(id));
		} else {
			cmds = commands;
		}
		ModuleThread m = modules.get(id);
		if (m != null) {
			m.pushCommandInput(cmds);
		}
	}

	public void sendDataFrom(int id, Map<Integer, List<Document>> data) {
		for (Entry<Integer, List<Document>> entry : data.entrySet()) {
			if (!blockedDataFrom.containsKey(id) || !blockedDataFrom.get(id).contains(entry.getKey())) {
				Map<Integer, Set<ModuleSlot>> map = routeData.get(id);
				if (map != null) {
					for (ModuleSlot moduleSlot : routeData.get(id).get(entry.getKey())) {
						Map<Integer, List<Document>> docs = new HashMap<Integer, List<Document>>();
						docs.put(moduleSlot.slot, entry.getValue());
						sendDataTo(moduleSlot.module, docs);
					}
				}
			}
		}
	}
	
	public void sendDataTo(int id, Map<Integer, List<Document>> data) {
		Map<Integer, List<Document>> dat;
		if (blockedCommandTo.containsKey(id)) {
			dat = new HashMap<Integer, List<Document>>();
			Set<Integer> set = blockedDataTo.get(id);
			for (Entry<Integer, List<Document>> entry : data.entrySet()) {
				if (!set.contains(entry.getKey())) {
					dat.put(entry.getKey(), entry.getValue());
				}
			}
		} else {
			dat = data;
		}
		ModuleThread m = modules.get(id);
		if (m != null) {
			m.pushDataInput(dat);
		}
	}
	
	public void sendCommandFromNB(int id, Set<Integer> commands) {
		for (int command : commands) {
			Map<Integer, Set<ModuleSlot>> map = routeCommand.get(id);
			if (map != null) {
				for (ModuleSlot moduleSlot : routeCommand.get(id).get(command)) {
					Set<Integer> set = new HashSet<Integer>();
					set.add(moduleSlot.slot);
					sendCommandTo(moduleSlot.module, set);
				}
			}
		}
	}
	
	public void sendCommandToNB(int id, Set<Integer> commands) {
		ModuleThread m = modules.get(id);
		if (m != null) {
			m.pushCommandInput(commands);
		}
	}

	public void sendDataFromNB(int id, Map<Integer, List<Document>> data) {
		for (Entry<Integer, List<Document>> entry : data.entrySet()) {
			Map<Integer, Set<ModuleSlot>> map = routeData.get(id);
			if (map != null) {
				for (ModuleSlot moduleSlot : routeData.get(id).get(entry.getKey())) {
					Map<Integer, List<Document>> docs = new HashMap<Integer, List<Document>>();
					docs.put(moduleSlot.slot, entry.getValue());
					sendDataTo(moduleSlot.module, docs);
				}
			}
		}
	}
	
	public void sendDataToNB(int id, Map<Integer, List<Document>> data) {
		ModuleThread m = modules.get(id);
		if (m != null) {
			m.pushDataInput(data);
		}
	}
	
	public void blockCommandFrom(int module, Set<Integer> slots) {
		if (!blockedCommandFrom.containsKey(module)) {
			blockedCommandFrom.put(module, new HashSet<Integer>());
		}
		blockedCommandFrom.get(module).addAll(slots);
	}
	
	public void blockCommandTo(int module, Set<Integer> slots) {
		if (!blockedCommandTo.containsKey(module)) {
			blockedCommandTo.put(module, new HashSet<Integer>());
		}
		blockedCommandTo.get(module).addAll(slots);
	}
	
	public void blockDataFrom(int module, Set<Integer> slots) {
		if (!blockedDataFrom.containsKey(module)) {
			blockedDataFrom.put(module, new HashSet<Integer>());
		}
		blockedDataFrom.get(module).addAll(slots);
	}
	
	public void blockDataTo(int module, Set<Integer> slots) {
		if (!blockedDataTo.containsKey(module)) {
			blockedDataTo.put(module, new HashSet<Integer>());
		}
		blockedDataTo.get(module).addAll(slots);
	}
	
	public void unblockCommandFrom(int module, Set<Integer> slots) {
		if (blockedCommandFrom.containsKey(module)) {
			blockedCommandFrom.get(module).removeAll(slots);
			if (blockedCommandFrom.get(module).isEmpty()) {
				blockedCommandFrom.remove(module);
			}
		}
	}
	
	public void unblockCommandTo(int module, Set<Integer> slots) {
		if (blockedCommandTo.containsKey(module)) {
			blockedCommandTo.get(module).removeAll(slots);
			if (blockedCommandTo.get(module).isEmpty()) {
				blockedCommandTo.remove(module);
			}
		}
	}
	
	public void unblockDataFrom(int module, Set<Integer> slots) {
		if (blockedDataFrom.containsKey(module)) {
			blockedDataFrom.get(module).removeAll(slots);
			if (blockedDataFrom.get(module).isEmpty()) {
				blockedDataFrom.remove(module);
			}
		}
	}
	
	public void unblockDataTo(int module, Set<Integer> slots) {
		if (blockedDataTo.containsKey(module)) {
			blockedDataTo.get(module).removeAll(slots);
			if (blockedDataTo.get(module).isEmpty()) {
				blockedDataTo.remove(module);
			}
		}
	}

	public Document query(int id, Document request) {
		if (modules.containsKey(id)) {
			return modules.get(id).query(request);
		}
		return null;
	}
	
}
