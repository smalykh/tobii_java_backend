package io.ccs.service.core.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;

import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;
import io.ccs.service.core.Util;

public class ModuleA implements Module {

	@Override
	public void run(ModuleAPI api) {
		try {
			// Every <interval> ms:
			// - command 0
			// - data 0 (<text>)
			Map<String, String> configMap = Util.documentToMap(api.getConfig());
			int interval = Integer.parseInt(configMap.get("interval"));
			String text = configMap.get("text");
			System.out.println("A: interval: " + interval);
			System.out.println("A: text: " + text);
			
			Set<Integer> outCommand = new HashSet<Integer>();
			outCommand.add(0);
			
			Map<String, String> dat = new HashMap<String, String>();
			dat.put("text", text);
			List<Document> list = new ArrayList<Document>();
			list.add(Util.mapToDocument(dat));
			Map<Integer, List<Document>> outData = new HashMap<Integer, List<Document>>();
			outData.put(0, list);
			
			while (true) {
				Thread.sleep(interval);
				api.pushCommandOutput(outCommand);
				api.pushDataOutput(outData);
				System.out.println("A: out 0");
			}
		} catch (InterruptedException e) {
			System.out.println("A: Interrupted");
		}
		
	}

	@Override
	public Document query(Document request) {
		return null;
	}
	
}
