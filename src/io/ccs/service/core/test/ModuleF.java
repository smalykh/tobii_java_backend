package io.ccs.service.core.test;

import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleFactory;

public class ModuleF implements ModuleFactory {
	
	@Override
	public Module make(int type) {
		switch (type) {
		case 0:
			return new ModuleA();
		case 1:
			return new ModuleB();
		case 2:
			return new ModuleC();
		default:
			return null;
		}
	}

}
