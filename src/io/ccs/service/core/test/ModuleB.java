package io.ccs.service.core.test;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;

import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;
import io.ccs.service.core.Util;

public class ModuleB implements Module {
	
	@Override
	public void run(ModuleAPI api) {
		try {
			// Wait <interval> ms:
			// - command 0
			// - if got command 0 -> command 1
			Map<String, String> configMap = Util.documentToMap(api.getConfig());
			int interval = Integer.parseInt(configMap.get("interval"));
			System.out.println("B: interval: " + interval);
			
			while (true) {
				Set<Integer> in = api.popCommandInputWait(interval);
				Set<Integer> out = new HashSet<Integer>();
				out.add(0);
				System.out.println("B: out 0");
				if (in.contains(0)) {
					out.add(1);
					System.out.println("B: out 1");
				}
				api.pushCommandOutput(out);
			}
		} catch (InterruptedException e) {
			System.out.println("B: Interrupted");
		}		
	}

	@Override
	public Document query(Document request) {
		return null;
	}

}
