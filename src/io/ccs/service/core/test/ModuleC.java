package io.ccs.service.core.test;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.w3c.dom.Document;

import io.ccs.service.core.Module;
import io.ccs.service.core.ModuleAPI;
import io.ccs.service.core.Util;

public class ModuleC implements Module {
	
	@Override
	public void run(ModuleAPI api) {
		try {
			// Every <interval> ms read input
			Map<String, String> configMap = Util.documentToMap(api.getConfig());
			int interval = Integer.parseInt(configMap.get("interval"));
			System.out.println("C: interval: " + interval);
			while (true) {
				Thread.sleep(interval);
				Set<Integer> in = api.popCommandInputPoll();
				for (Integer i : in) {
					System.out.println("C: in " + i.toString());
				}
				Map<Integer, List<Document>> map = api.popDataInputPoll();
				for (Entry<Integer, List<Document>> entry : map.entrySet()) {
					String str = "C: in data: slot " + entry.getKey() + ": ";
					for (Document doc : entry.getValue()) {
						Map<String, String> dat = Util.documentToMap(doc);
						str += "(";
						for (Entry<String, String> entr2 : dat.entrySet()) {
							str += entr2.getKey() + ": " + entr2.getValue() + "; ";
						}
						str += "), ";
					}
					System.out.println(str);
				}
			}
		} catch (InterruptedException e) {
			System.out.println("C: Interrupted");
		}
	}

	@Override
	public Document query(Document request) {
		return null;
	}

}
