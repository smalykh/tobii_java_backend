package io.ccs.service.core.test;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import io.ccs.service.core.Core;

public class Test {

	private static String xmlFileName = "test.xml";
	
	public static void main(String[] args) {
		
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document config = builder.parse(new File(xmlFileName));
			Core core = new Core(config, new ModuleF());
			System.out.println("Start");
			core.start();
			Thread.sleep(10000);
			System.out.println("Interrupt");
			core.interrupt();
			System.out.println("Join");
			core.join();
		} catch (ParserConfigurationException | SAXException | IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("End");

	}

}
