package io.ccs.service.core;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.compression.ZlibCodecFactory;
import io.netty.handler.codec.compression.ZlibWrapper;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.codec.xml.XmlFrameDecoder;
import io.netty.handler.ssl.SslContext;
import io.netty.util.CharsetUtil;

public class NetworkInitializer extends ChannelInitializer<SocketChannel> {
	
	private final SslContext sslCtx;
	private final Router router;
	
	public NetworkInitializer(SslContext sslCtx, Router router) {
		this.sslCtx = sslCtx;
		this.router = router;
	}
	
	@Override
	public void initChannel(SocketChannel ch) {
		ChannelPipeline pipeline = ch.pipeline();
		if (sslCtx != null) {
			pipeline.addLast(sslCtx.newHandler(ch.alloc()));
		}
		pipeline.addLast(ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));
		pipeline.addLast(ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));
		pipeline.addLast(new XmlFrameDecoder(Integer.MAX_VALUE));
		pipeline.addLast(new StringDecoder(CharsetUtil.UTF_8));
		pipeline.addLast(new StringEncoder(CharsetUtil.UTF_8));
		pipeline.addLast(new NetworkHandler(router));
	}
	
}