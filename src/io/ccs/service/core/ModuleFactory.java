package io.ccs.service.core;

public interface ModuleFactory {
	
	public Module make(int type);

}
