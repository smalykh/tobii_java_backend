package io.ccs.service.core;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Core {
	
	private Router router = null;
	private Network network = null;
	
	public Core(Document config, ModuleFactory moduleFactory) {
		Element configElement = config.getDocumentElement();
		Element configRouterElement = (Element) configElement.getElementsByTagName("router").item(0);
		Element configNetworkElement = (Element) configElement.getElementsByTagName("network").item(0);
		Document configRouterDocument = Util.elementToDocument(configRouterElement);
		Document configNetworkDocument = Util.elementToDocument(configNetworkElement);
		this.router = new Router(configRouterDocument, moduleFactory);
		this.network = new Network(configNetworkDocument, router);
	}
	
	public void start() {
		router.start();
		network.start();
	}
	
	public void interrupt() {
		router.interrupt();
		network.interrupt();
	}
	
	public void join() throws InterruptedException {
		router.join();
		network.join();
	}

}
