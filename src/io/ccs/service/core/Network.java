package io.ccs.service.core;

import java.util.logging.Logger;

import org.w3c.dom.Document;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

public class Network extends Thread {
	
	private final Document config;
	private final Router router;
	
	public Network(Document config, Router router) {
		this.config = config;
		this.router = router;
	}
	
	@Override
	public void run() {
		if (config == null) {
			Logger.getLogger(this.getClass().getName()).info("No config, lol");
		}
		if (router == null) {
			Logger.getLogger(this.getClass().getName()).info("No router, lol");
		}
		
		EventLoopGroup bossGroup = new NioEventLoopGroup(1);
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
			.channel(NioServerSocketChannel.class)
			.handler(new LoggingHandler(LogLevel.INFO))
			.childHandler(new NetworkInitializer(null, router));
			int PORT = 31337;
			b.bind(PORT).sync().channel().closeFuture().sync();
		} catch (InterruptedException e) {
			System.out.println("Server ended");
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
		
	}

}
