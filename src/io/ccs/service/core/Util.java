package io.ccs.service.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Util {
	
	public static Document cloneDocument(Document document) {
		return elementToDocument(document.getDocumentElement());
	}
	
	public static Document elementToDocument(Element element) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			return null;
		}
		Document document = builder.newDocument();
		document.appendChild(document.importNode(element, true));
		return document;
	}
	
	public static Map<String, String> documentToMap(Document document) {
		return elementToMap(document.getDocumentElement());
	}
	
	public static Map<String, String> documentToMap(Document document, String tag, String name) {
		return elementToMap(document.getDocumentElement(), tag, name);
	}
	
	public static Map<String, String> elementToMap(Element element) {
		return elementToMap(element, "value", "name");
	}
	
	public static Map<String, String> elementToMap(Element element, String tag, String name) {
		Map<String, String> map = new HashMap<String, String>();
		NodeList nodeList = element.getElementsByTagName(tag);
		for (int i = 0; i < nodeList.getLength(); ++i) {
			Element e = (Element) nodeList.item(i);
			map.put(e.getAttribute(name), e.getTextContent());
		}
		return map;
	}
	
	public static Document mapToDocument(Map<String, String> map) {
		return mapToDocument(map, "data", "value", "name");
	}
	
	public static Document mapToDocument(Map<String, String> map, String root, String tag, String name) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			return null;
		}
		Document document = builder.newDocument();
		Element rootElement = document.createElement(root);
		document.appendChild(rootElement);
		for (Entry<String, String> entry : map.entrySet()) {
			Element element = document.createElement(tag);
			rootElement.appendChild(element);
			Attr attr = document.createAttribute(name);
			attr.setValue(entry.getKey());
			element.setAttributeNode(attr);
			element.appendChild(document.createTextNode(entry.getValue()));
		}
		return document;
	}
	
	public static Element mapToElement(Map<String, String> map) {
		return mapToDocument(map).getDocumentElement();
	}
	
	public static Element mapToElement(Map<String, String> map, String root, String tag, String name) {
		return mapToDocument(map, root, tag, name).getDocumentElement();
	}
	
}
