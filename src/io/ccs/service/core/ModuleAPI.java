package io.ccs.service.core;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Document;

public class ModuleAPI {

	private final ModuleThread moduleThread;
	
	public ModuleAPI(ModuleThread moduleThread) {
		if (moduleThread == null) {
			throw new IllegalArgumentException();
		}
		this.moduleThread = moduleThread;
	}
	
	public Document getConfig() {
		return moduleThread.getConfig();
	}
	
	public void pushCommandOutput(Set<Integer> commands) {
		moduleThread.pushCommandOutput(commands);
	}
	
	public void pushDataOutput(Map<Integer, List<Document>> data) {
		moduleThread.pushDataOutput(data);
	}
	
	public Set<Integer> popCommandInputPoll() {
		return moduleThread.popCommandInputPoll();
	}
	
	public Set<Integer> popCommandInputWait() throws InterruptedException {
		return moduleThread.popCommandInputWait();
	}
	
	public Set<Integer> popCommandInputWait(long timeout) throws InterruptedException {
		return moduleThread.popCommandInputWait(timeout);
	}
	
	public Set<Integer> popCommandInputWait(long timeout, int nanos) throws InterruptedException {
		return moduleThread.popCommandInputWait(timeout, nanos);
	}
	
	public Map<Integer, List<Document>> popDataInputPoll() {
		return moduleThread.popDataInputPoll();
	}
	
	public Map<Integer, List<Document>> popDataInputWait() throws InterruptedException {
		return moduleThread.popDataInputWait();
	}
	
	public Map<Integer, List<Document>> popDataInputWait(long timeout) throws InterruptedException {
		return moduleThread.popDataInputWait(timeout);
	}
	
	public Map<Integer, List<Document>> popDataInputWait(long timeout, int nanos) throws InterruptedException {
		return moduleThread.popDataInputWait(timeout, nanos);
	}
	
}
