package io.ccs.service.core;

import org.w3c.dom.Document;

public interface Module {
	
	public void run(ModuleAPI api);
	public Document query(Document request);

}
