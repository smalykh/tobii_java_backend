package io.ccs.service.core;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class NetworkHandler extends SimpleChannelInboundHandler<String> {
	
	private final Router router;
	
	public NetworkHandler(Router router) {
		this.router = router;
		Logger.getLogger(getClass().getName()).info("Client connected");
	}

	@Override
	public void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
		Logger.getLogger(getClass().getName()).info("Request received");
		System.out.println("Request: " + msg);
		// Parse XML of Request Packet
		DocumentBuilderFactory factoryRequestPacket = DocumentBuilderFactory.newInstance();  
		DocumentBuilder builderRequestPacket;
	    builderRequestPacket = factoryRequestPacket.newDocumentBuilder(); 
	    Document documentRequestPacket = builderRequestPacket.parse(new InputSource(new StringReader(msg)));
	    // Only "packet" documents are acceptable
	    if (documentRequestPacket.getDocumentElement().getNodeName().equals("packet")) {
	    	// Make XML of Response Packet
	    	DocumentBuilderFactory factoryResponsePacket = DocumentBuilderFactory.newInstance();  
			DocumentBuilder builderResponsePacket;
		    builderResponsePacket = factoryResponsePacket.newDocumentBuilder(); 
		    Document documentResponsePacket = builderResponsePacket.newDocument();
		    Element rootElementResponsePacket = documentResponsePacket.createElement("packet");
		    documentResponsePacket.appendChild(rootElementResponsePacket);
	    	// Iterate over requests
	    	NodeList nodeList = documentRequestPacket.getDocumentElement().getElementsByTagName("request");
	    	for (int i = 0; i < nodeList.getLength(); ++i) {
				Element e = (Element) nodeList.item(i);
				String command = e.getAttribute("command");
				// Add empty response
				Element elementResponse = documentResponsePacket.createElement("response");
				rootElementResponsePacket.appendChild(elementResponse);
				elementResponse.setAttribute("id", e.getAttribute("id"));
				// Command switch
				if (command.equals("block")) {
					String type = e.getAttribute("type");
					String direction = e.getAttribute("direction");
					int module = Integer.parseInt(e.getAttribute("module"));
					int slot = Integer.parseInt(e.getAttribute("slot"));
					Set<Integer> set = new HashSet<Integer>();
					set.add(slot);
					if (type.equals("command")) {
						if (direction.equals("from")) {
							router.blockCommandFrom(module, set);
						} else if (direction.equals("to")) {
							router.blockCommandTo(module, set);
						}
					} else if (type.equals("data")) {
						if (direction.equals("from")) {
							router.blockDataFrom(module, set);
						} else if (direction.equals("to")) {
							router.blockDataTo(module, set);
						}
					}
				} else if (command.equals("unblock")) {
					String type = e.getAttribute("type");
					String direction = e.getAttribute("direction");
					int module = Integer.parseInt(e.getAttribute("module"));
					int slot = Integer.parseInt(e.getAttribute("slot"));
					Set<Integer> set = new HashSet<Integer>();
					set.add(slot);
					if (type.equals("command")) {
						if (direction.equals("from")) {
							router.unblockCommandFrom(module, set);
						} else if (direction.equals("to")) {
							router.unblockCommandTo(module, set);
						}
					} else if (type.equals("data")) {
						if (direction.equals("from")) {
							router.unblockDataFrom(module, set);
						} else if (direction.equals("to")) {
							router.unblockDataTo(module, set);
						}
					}
				} else if (command.equals("send")) {
					String type = e.getAttribute("type");
					boolean nb = Boolean.parseBoolean(e.getAttribute("nb"));
					String direction = e.getAttribute("direction");
					int module = Integer.parseInt(e.getAttribute("module"));
					int slot = Integer.parseInt(e.getAttribute("slot"));
					Set<Integer> set = new HashSet<Integer>();
					set.add(slot);
					if (type.equals("command")) {
						if (nb) {
							if (direction.equals("from")) {
								router.sendCommandFromNB(module, set);
							} else if (direction.equals("to")) {
								router.sendCommandToNB(module, set);
							}
						} else {
							if (direction.equals("from")) {
								router.sendCommandFrom(module, set);
							} else if (direction.equals("to")) {
								router.sendCommandTo(module, set);
							}
						}
					} else if (type.equals("data")) {
						// TODO: implement!!!!
					}
				} else if (command.equals("query")) {
					Element req = (Element) e.getElementsByTagName("request").item(0);
					Document reqDoc = Util.elementToDocument(req);
					Document respDoc = router.query(Integer.parseInt(e.getAttribute("module")), reqDoc);
					if (respDoc != null) {
						elementResponse.appendChild(documentResponsePacket.importNode(respDoc.getDocumentElement(), true));
					}
				}
	    	}
	    	DOMSource domSourceResponsePacket = new DOMSource(documentResponsePacket);
	    	StringWriter stringWriterResponsePacket = new StringWriter();
	    	StreamResult streamResultResponsePacket = new StreamResult(stringWriterResponsePacket);
	    	TransformerFactory transformerFactoryResponsePacket = TransformerFactory.newInstance();
	    	Transformer transformerResponsePacket = transformerFactoryResponsePacket.newTransformer();
	    	transformerResponsePacket.transform(domSourceResponsePacket, streamResultResponsePacket);
	    	ctx.writeAndFlush(stringWriterResponsePacket.toString());
	    }
		//ctx.writeAndFlush(msg.retain());
		//router.sendCommandTo(0, new HashSet<Integer>());
	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		Logger.getLogger(getClass().getName()).info("Client disconnected");
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
	
}
