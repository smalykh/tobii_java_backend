package io.ccs.service.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.w3c.dom.Document;

public class ModuleThread extends Thread {

	private final Object monitorCommand = new Object();
	private final Object monitorData = new Object();
	private Set<Integer> commands = new HashSet<Integer>();
	private Map<Integer, List<Document>> data = new HashMap<Integer, List<Document>>();
	
	private final Router router;
	private final int id;
	private final Module module;
	private final Document config;
	
	public ModuleThread(Router router, int id, Module module, Document config) {
		this.router = router;
		this.id = id;
		this.module = module;
		this.config = config;
	}
		
	@Override
	public void run() {
		module.run(new ModuleAPI(this));
	}
	
	public Document query(Document request) {
		return module.query(request);
	}

	public Document getConfig() {
		return config;
	}
	
	public void pushCommandOutput(Set<Integer> commands) {
		router.sendCommandFrom(id, commands);
	}

	public void pushDataOutput(Map<Integer, List<Document>> data) {
		router.sendDataFrom(id, data);
	}
	
	public void pushCommandInput(Set<Integer> commands) {
		synchronized (monitorCommand) {
			this.commands.addAll(commands);
			monitorCommand.notify();
		}
	}

	public void pushDataInput(Map<Integer, List<Document>> data) {
		synchronized (monitorData) {
			for (Entry<Integer, List<Document>> entry : data.entrySet()) {
				List<Document> documents;
				if (this.data.containsKey(entry.getKey())) {
					documents = this.data.get(entry.getKey());
				} else {
					documents = new ArrayList<Document>();
					this.data.put(entry.getKey(), documents);
				}
				for (Document document : entry.getValue()) {
					documents.add(Util.cloneDocument(document));
				}
			}
			monitorData.notify();
		}
	}

	public Set<Integer> popCommandInputPoll() {
		Set<Integer> oldCommands;
		synchronized (monitorCommand) {
			oldCommands = commands;
			commands = new HashSet<Integer>();
		}
		return oldCommands;
	}

	public Set<Integer> popCommandInputWait() throws InterruptedException {
		Set<Integer> oldCommands = null;
		synchronized (monitorCommand) {
			if (commands.isEmpty()) {
				monitorCommand.wait();
			}
			oldCommands = commands;
			commands = new HashSet<Integer>();
		}
		return oldCommands;
	}

	public Set<Integer> popCommandInputWait(long timeout) throws InterruptedException {
		Set<Integer> oldCommands = null;
		synchronized (monitorCommand) {
			if (commands.isEmpty()) {
				monitorCommand.wait(timeout);
			}
			oldCommands = commands;
			commands = new HashSet<Integer>();
		}
		return oldCommands;
	}

	public Set<Integer> popCommandInputWait(long timeout, int nanos) throws InterruptedException {
		Set<Integer> oldCommands = null;
		synchronized (monitorCommand) {
			if (commands.isEmpty()) {
				monitorCommand.wait(timeout, nanos);
			}
			oldCommands = commands;
			commands = new HashSet<Integer>();
		}
		return oldCommands;
	}

	public Map<Integer, List<Document>> popDataInputPoll() {
		Map<Integer, List<Document>> oldData;
		synchronized (monitorData) {
			oldData = data;
			data = new HashMap<Integer, List<Document>>();
		}
		return oldData;
	}

	public Map<Integer, List<Document>> popDataInputWait() throws InterruptedException {
		Map<Integer, List<Document>> oldData = null;
		synchronized (monitorData) {
			if (data.isEmpty()) {
				monitorData.wait();
			}
			oldData = data;
			data = new HashMap<Integer, List<Document>>();
		}
		return oldData;
	}

	public Map<Integer, List<Document>> popDataInputWait(long timeout) throws InterruptedException {
		Map<Integer, List<Document>> oldData = null;
		synchronized (monitorData) {
			if (data.isEmpty()) {
				monitorData.wait(timeout);
			}
			oldData = data;
			data = new HashMap<Integer, List<Document>>();
		}
		return oldData;
	}

	public Map<Integer, List<Document>> popDataInputWait(long timeout, int nanos) throws InterruptedException {
		Map<Integer, List<Document>> oldData = null;
		synchronized (monitorData) {
			if (data.isEmpty()) {
				monitorData.wait(timeout, nanos);
			}
			oldData = data;
			data = new HashMap<Integer, List<Document>>();
		}
		return oldData;
	}
	
}
